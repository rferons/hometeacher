<?php

use Phinx\Migration\AbstractMigration;

class CreateUserTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
    public function change()
    {
    }
    */
    
    /**
     * Migrate Up.
     */
    public function up()
    {
        $table = $this->table('users', ['id' => 'user_id']);
        $table
            ->addColumn('unit_id', 'integer', ['null' => false])
            ->addColumn('name', 'string', ['null' => false, 'limit' => 64])
            ->addColumn('email', 'string', ['null' => false, 'limit' => 128])
            ->addColumn('username', 'string', ['null' => false, 'limit' => 32])
            ->addColumn('password', 'string', ['null' => false, 'limit' => 64])
            ->addColumn('mustChangePassword', 'boolean', ['null' => true, 'default' => 0])
            ->addColumn('active', 'boolean', ['null' => true, 'default' => 0])
            ->addColumn('created_date', 'timestamp', [ 'default' => 'CURRENT_TIMESTAMP' ])
            ->addIndex('unit_id')
            ->addIndex('username')
            ->addIndex('password')
            ->addIndex('email')
            ->save();
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->dropTable('users');
    }
}