<?php

use Phinx\Migration\AbstractMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class CreateUnitsTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
    public function change()
    {
    }
    */
    
    /**
     * Migrate Up.
     */
    public function up()
    {
        $table = $this->table('units', ['id' => 'unit_id']);
        $table
            ->addColumn('unit_unit_id', 'integer', ['null' => true, 'default' => 0])
            ->addColumn('name', 'string', ['null' => false, 'limit' => 64])
            ->addColumn('number', 'integer', ['null' => true])
            ->addColumn('type_flag', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_TINY])
            ->addColumn('created_date', 'timestamp', ['default' => 'CURRENT_TIMESTAMP'])
            ->addIndex('unit_id')
            ->addIndex('name')
            ->addIndex('type_flag')
            ->save();
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->dropTable('units');
    }
}