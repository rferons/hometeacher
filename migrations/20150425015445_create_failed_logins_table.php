<?php

use Phinx\Migration\AbstractMigration;

class CreateFailedLoginsTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
    public function change()
    {
    }
    */
    
    /**
     * Migrate Up.
     */
    public function up()
    {
        $this
            ->table('failed_logins', ['failed_login_id'])
            ->addColumn('user_id', 'integer', ['null' => false])
            ->addColumn('ip_address', 'string', ['null' => false, 'limit' => 15])
            ->addColumn('attempted', 'integer', ['null' => false, 'limit' => 5])
            ->save();
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->dropTable('failed_logins');
    }
}