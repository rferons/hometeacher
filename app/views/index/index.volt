<!-- Main jumbotron for a primary marketing message or call to action -->
<div class="jumbotron">
    <div class="container-fluid">
        <h1>Home Teaching</h1>
        <p>Make managing and reporting home teaching simpler</p>
        <p><a class="btn btn-primary btn-lg" href="/signup" role="button">Sign Up &raquo;</a></p>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <h2>Simple</h2>
        <p>Reporting home teaching can be a difficult task. HomeTeaching.org makes it simple! Every month an email goes out
            with a link to a unique, secure web page where results can be reported.
        </p>
        <p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>
    </div>
    <div class="col-md-4">
        <h2>Powerful</h2>
        <p>Always know who is being visited an who hasn't received a visit in a while with our powerful and smart dashboard.
             It gives you the information you need when you need it.
        </p>
        <p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>
    </div>
    <div class="col-md-4">
        <h2>Available Everywhere</h2>
        <p>HomeTeaching.org has been built using modern technologies which makes it accessible on any device. Use it on
            your smartphone, tablet, laptop or computer.
        </p>
        <p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-sm-12">
        <div class="page-header">
            <h2>HomeTeaching.org provides four key benefits</h2>
            <ol>
                <li>Quorum Leaders can administer their home teaching program from any computer that has internet access. Multiple users can work on the same information simultaneiously. The Ward Clerk can assign new families to a quorum. The quorum leaders can make home teaching assignments.</li>
                <li>Home Teaching reporting can be done automatically. Periodically, an email message is sent to the Home Teachers who can click on a link and report their home teaching results on line. This eliminates the need to call them for their report.</li>
                <li>You can assign home teachers to families who's records have not yet been received by the ward. This allows new families to receive home teachers earlier.</li>
                <li>A number of reports facilitate efficient home teaching, such as reports on the single sister's home teaching or a list of families that need a new home teacher or a report of home teachers who have not reported results.</li>
            </ol>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="page-header">
            <h2>HomeTeaching.org is a free site.</h2>
            <p> Its purpose is to simplify the administration of the Home Teaching program of the Church of Jesus Christ of Latter-day Saints.
                This site has no commercial purpose. It's information is not shared, nor is there any advertising.</p>
            <p>The sole purpose is to allow quorum leaders to fulfill their administrative responsibilities efficiently so that they will be more available for ministering to those under their stewardship.
                This service is designed to work in conjunction with the MLS system.</p>
            <p>HomeTeaching.org will make managing the home teaching of your ward and quorum much easier.</p>
        </div>
        {#<img class="img-responsive img-rounded pull-right" src="/img/home-teaching.jpg" alt="Picture of family being hometaught" />#}
    </div>
</div>
