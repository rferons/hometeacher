<div class="navbar">
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/">Home Teaching</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                {{ form('/session/signin', 'action' : 'post', 'class': 'navbar-form navbar-right') }}
                    <div class="form-group">
                        {{ text_field('username', 'placeholder' : 'Username', 'class' : 'form-control input-sm', 'required': true) }}
                    </div>
                    <div class="form-group">
                        {{ password_field('password', 'placeholder' : 'Pasword', 'class' : 'form-control input-sm', 'required': true) }}
                    </div>
                    {{ submit_button('Sign In', 'class' : 'btn btn-default btn-sm') }}
                {{ endform() }}
            </div><!--/.navbar-collapse -->
        </div>
    </nav>
</div>

<div class="container-fluid main-container">
    <div id='flash-session-div'>flash output here : {{ flashSession.output() }}</div>
    {{ content() }}
</div>

<footer class="footer">
    <div class="container-fluid">
        Made with love &nbsp;<i class="fa fa-heart"></i> |
        {{ link_to("privacy", "Privacy Policy") }} |
        {{ link_to("terms", "Terms") }}
    </div>
</footer>
