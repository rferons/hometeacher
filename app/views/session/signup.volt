{{ form('signup', 'method':'post') }}
<div class="row">
    <div class="col-sm-6 col-sm-offset-3">

        <h2>Find your stake</h2>
        <p>Start typing the name of your stake. If it in our system then it will appear as you type. Select it if you see it.
            If you do not see your stake appear then continue to type in the name of your stake.</p>
        {{ form.render('stake') }}
        <h2>Find your Ward</h2>
        <p>Do the same thing you did above. Start typing and if you see your ward select it. If you don't then continue
        typing the name of your ward and it will be created when you are done.</p>
        {{ form.render('ward') }}
        <h2>Add Your Organization</h2>
        {{ form.render('organization') }}
        <h2>User Information</h2>
        <label for="usr_name">Name</label>
        {{ form.render('name') }}
        <label for="usr_email">Email</label>
        {{ form.render('email') }}
        <label for="usr_username">Username</label>
        {{ form.render('username') }}
        <label for="usr_password">Password</label>
        {{ form.render('password') }}
        <label for="password_confirmation">Password Confirmation</label>
        {{ form.render('password_confirmation') }}
        <br>
        {{ form.render('csrf', ['value': security.getToken()]) }}
        {{ submit_button('submit', 'class':'btn btn-primary') }}
    </div>
</div>
{{ endform() }}
