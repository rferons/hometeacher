<div class="navbar">
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/">Home Teaching</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li>Private area</li>
                </ul>
            </div><!--/.navbar-collapse -->
        </div>
    </nav>
</div>

<div class="container-fluid main-container">
    <?php echo $this->getContent(); ?>
</div>

<footer class="footer">
    <div class="container-fluid">
        Made with love &nbsp;<i class="fa fa-heart"></i> |
        <?php echo $this->tag->linkTo(array('privacy', 'Privacy Policy')); ?> |
        <?php echo $this->tag->linkTo(array('terms', 'Terms')); ?>
    </div>
</footer>
