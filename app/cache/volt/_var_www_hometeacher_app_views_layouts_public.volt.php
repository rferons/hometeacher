<div class="navbar">
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/">Home Teaching</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <?php echo $this->tag->form(array('/session/signin', 'action' => 'post', 'class' => 'navbar-form navbar-right')); ?>
                    <div class="form-group">
                        <?php echo $this->tag->textField(array('username', 'placeholder' => 'Username', 'class' => 'form-control input-sm', 'required' => true)); ?>
                    </div>
                    <div class="form-group">
                        <?php echo $this->tag->passwordField(array('password', 'placeholder' => 'Pasword', 'class' => 'form-control input-sm', 'required' => true)); ?>
                    </div>
                    <?php echo $this->tag->submitButton(array('Sign In', 'class' => 'btn btn-default btn-sm')); ?>
                <?php echo $this->tag->endform(); ?>
            </div><!--/.navbar-collapse -->
        </div>
    </nav>
</div>

<div class="container-fluid main-container">
    <div id='flash-session-div'>flash output here : <?php echo $this->flashSession->output(); ?></div>
    <?php echo $this->getContent(); ?>
</div>

<footer class="footer">
    <div class="container-fluid">
        Made with love &nbsp;<i class="fa fa-heart"></i> |
        <?php echo $this->tag->linkTo(array('privacy', 'Privacy Policy')); ?> |
        <?php echo $this->tag->linkTo(array('terms', 'Terms')); ?>
    </div>
</footer>
