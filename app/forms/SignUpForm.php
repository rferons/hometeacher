<?php
namespace Hometeacher\Forms;

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Password;
use Phalcon\Forms\Element\Submit;
use Phalcon\Forms\Element\Check;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\Identical;
use Phalcon\Validation\Validator\StringLength;
use Phalcon\Validation\Validator\Confirmation;


class SignUpForm extends Form
{
    public function initialize($entity = null, $options = null)
    {
        /**
         * Stake
         */
        $stake = new Text(
            'stake',
            [
                'class'         => 'form-control input-sm',
                'placeholder'   => 'Ashburn, Virginia Stake',
                'name'          => 'stake'
            ]
        );
        $stake->addValidators([
            new PresenceOf([
                'message' => 'Stake is required'
            ])
        ]);
        $this->add($stake);

        /**
         * ward
         */
        $ward = new Text(
            'ward',
            [
                'class'         => 'form-control input-sm',
                'placeholder'   => 'Leesburg Ward',
                'name'          => 'ward'
            ]
        );
        $ward->addValidators([
            new PresenceOf([
                'message' => 'ward is required'
            ])
        ]);
        $this->add($ward);

        /**
         * organization
         */
        $organization = new Text(
            'organization',
            [
                'class'         => 'form-control input-sm',
                'placeholder'   => 'Elders Quorum',
                'name'          => 'organization'
            ]
        );
        $organization->addValidators([
            new PresenceOf([
                'message' => 'organization is required'
            ])
        ]);
        $this->add($organization);

        /**
         * Name
         */
        $name = new Text(
            'name',
            [
                'class'         => 'form-control input-sm',
                'placeholder'   => 'John Doe',
                'name'          => 'name'
            ]
        );
        $name->setLabel('Name');
        $name->addValidators([
            new PresenceOf([
                'message' => 'The name is required'
            ])
        ]);
        $this->add($name);

        /**
         * Email
         */
        $email = new Text(
            'email',
            [
                'class'         => 'form-control input-sm',
                'placeholder'   => 'john.doe@gmail.com',
                'name'          => 'email'
            ]
        );
        $email->setLabel('Email');
        $email->addValidators([
            new PresenceOf([
                'message'   => 'Email is required'
            ]),
            new Email([
                'message'   => 'Email must be a valid email'
            ])
        ]);
        $this->add($email);

        /**
         * Username
         */
        $username = new Text(
            'username',
            [
                'class'         => 'form-control input-sm',
                'placeholder'   => 'john.doe@gmail.com',
                'name'          => 'username'
            ]
        );
        $username->setLabel('Username');
        $username->addValidators([
           new PresenceOf([
               'message'    => 'username is required'
           ])
        ]);
        $this->add($username);

        /**
         * Password
         */
        $password = new Password(
            'password',
            [
                'class' => 'form-control input-sm'
            ]
        );
        $password->setLabel('Password');
        $password->addValidators([
            new PresenceOf([
                'message'   => 'Password is required'
            ]),
            new StringLength([
                'min' => 8,
                'messageMinimum' => 'Password is too short. Minimum 8 characters'
            ]),
            new Confirmation([
                'message'   => 'Password doesn\'t match confirmation.',
                'with'      => 'password_confirmation'
            ])
        ]);
        $this->add($password);

        /**
         * Password Confirmation
         */
        $password_conf = new Password(
            'password_confirmation',
            [
                'class' => 'form-control input-sm'
            ]
        );
        $password_conf->setLabel('Confirm Password');
        $password_conf->addValidators([
            new PresenceOf([
                'message'   => 'confirm password is required'
            ])
        ]);
        $this->add($password_conf);

        // CSRF
        $csrf = new Hidden('csrf');

        $csrf->addValidator(new Identical(array(
            'value' => $this->security->getSessionToken(),
            'message' => 'CSRF validation failed'
        )));

        $this->add($csrf);

        // Sign Up
        $this->add(new Submit('Sign Up', array(
            'class' => 'btn btn-success'
        )));
    }

    /**
     * Prints messages for a specific element
     */
    public function messages($name)
    {
        if ($this->hasMessagesFor($name)) {
            foreach ($this->getMessagesFor($name) as $message) {
                $this->flash->error($message);
            }
        }
    }
}