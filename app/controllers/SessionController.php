<?php
namespace Hometeacher\Controllers;

use Hometeacher\Forms\SignUpForm;
use Hometeacher\Models\Users;
use Hometeacher\Models\Units;

class SessionController extends ControllerBase
{
    /**
     * Sets the private layout
     */
    public function initialize()
    {
        $this->view->setTemplateBefore('public');
    }

    public function signupAction()
    {
        $form = new SignUpForm();
        $this->view->setVar('form', $form);

        if ($this->request->isPost()) {

            $post = $this->request->getPost();

            // create the new user object and bind to the form
            $user = new Users();

            // Bind the form elements to the user object
            $form->bind($post, $user);

            // encrypt the password
            $user->password = $this->security->hash($this->password);

            // Set user active and set the created date
            $user->active = 1;
            $user->created_date = date('Y-m-d i:m:s');

            // If the form is valid then proceed
            if ($form->isValid($post)) {
                // find the stake
                $stake = Units::findFirst([
                    'name = :stake:',
                    'bind'  => [
                            'stake'  => $post['stake']
                        ]
                ]);
                if (!$stake) {
                    $stake = new Units();
                    $stake->type_flag = Units::TYPE_STAKE;
                    $stake->name = $post['stake'];
                }

                // find the ward
                $ward = $stake->getChildren([
                    'name = :ward:',
                    'bind'  => [
                        'ward'  => $post['ward']
                    ]
                ]);
                if (count($ward) < 1 ) {
                    $ward = new Units();
                    $ward->type_flag = Units::TYPE_WARD;
                    $ward->name = $post['ward'];
                }

                // Find the organization
                $org = $ward->getChildren([
                    'name = :org:',
                    'bind'  => [
                        'org'  => $post['organization']
                    ]
                ]);

                if (count($org) < 1 ) {
                    $org = new Units();
                    $org->type_flag = Units::TYPE_ORGANIZATION;
                    $org->name = $post['organization'];
                }

                // Save all the data using the model relationships
                $org->User = $user;
                $ward->Children = $org;
                $stake->Children = $ward;

                if ( $stake->save() ) {

                    $this->session->set('auth-identity', [
                        'user_id' => $user->user_id,
                        'name' => $user->name,
                        'organization' => $user->Organization
                    ]);

                    $this->flash->success('Account created successfully');

                    return $this->response->redirect('dashboard');

                } else {
                    $output = '';
                    foreach ($stake->getMessages() as $message) {
                        $output .= $message->getMessage() . "\n";
                    }
                    print $output;
                    $this->flash->error($output);
                }
            }
        }
    }

    public function signinAction()
    {
        if ($this->request->isPost()) {
            $credentials = [
                'username' => $this->request->getPost('username'),
                'password' => $this->request->getPost('password')
            ];

            $this->auth->check($credentials);

            die;
            return $this->response->redirect('dashboard');

        }
    }
}