<?php
namespace Hometeacher\Controllers;

use Hometeacher\Controllers\ControllerBase as HBase;

class IndexController extends HBase
{

    /**
     * Sets the private layout
     */
    public function initialize()
    {
        $this->view->setTemplateBefore('public');
    }

    /**
     * Default action
     */
    public function indexAction()
    {

    }
}

