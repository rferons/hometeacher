<?php
namespace Hometeacher;

class Model extends \Phalcon\Mvc\Model
{
    public function beforeCreate()
    {
        // Set the created date
        $column = $this->prefix . '_created-date';
        $this->$column = date('Y-m-d H:i:s');
    }
}