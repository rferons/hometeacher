<?php
namespace Hometeacher;

use Phalcon\Mvc\User\Component;
use Hometeacher\Models\Users;
use Hometeacher\Models\RememberTokens;
use Hometeacher\Models\SuccessLogins;
use Hometeacher\Models\FailedLogins;

/**
 * Hometeacher\Auth
 * Manages Authentication/Identity Management in Hometeacher
 */
class Auth extends Component
{

    /**
     * Checks the user credentials
     *
     * @param $credentials
     * @throws Exception
     */
    public function check($credentials)
    {
        try{
            print 'in the check function';

            // Check if the user exist
            $user = Users::findFirst("username = '" .$credentials['username']. "'");
            print 'user?';
            if ($user == false) {
                print 'no user found';
                var_dump($user);
                $this->registerUserThrottling(0);
                throw new Exception('Wrong email/password combination');
            }

            print_r($user->toArray());

            print_r($credentials);

            $password = $this->getDI()
                ->getSecurity()
                ->hash($credentials['password']);

            print ($password);

            // Check the password
            if (!$this->security->checkHash($credentials['password'], $user->password)) {
                print 'no match';
                die;
                $this->registerUserThrottling($user->user_id);
                throw new Exception('Wrong email/password combination');
            }

            print 'issue?';

            die;

            // Check if the user was flagged
            $this->checkUserFlags($user);

            $this->session->set('auth-identity', [
                'user_id' => $user->user_id,
                'name' => $user->name,
                'organization' => $user->Organization
            ]);
        } catch (\Exception $e) {
            print $e->getMessage();
        }

    }


    /**
     * Implements login throttling
     * Reduces the efectiveness of brute force attacks
     *
     * @param int $userId
     */
    public function registerUserThrottling($userId)
    {
        $failedLogin = new FailedLogins();
        $failedLogin->usersId = $userId;
        $failedLogin->ipAddress = $this->request->getClientAddress();
        $failedLogin->attempted = time();
        $failedLogin->save();

        $attempts = FailedLogins::count(array(
            'ipAddress = ?0 AND attempted >= ?1',
            'bind' => array(
                $this->request->getClientAddress(),
                time() - 3600 * 6
            )
        ));

        switch ($attempts) {
            case 1:
            case 2:
                // no delay
                break;
            case 3:
            case 4:
                sleep(2);
                break;
            default:
                sleep(4);
                break;
        }
    }

    /**
     * Checks if the user is banned/inactive/suspended
     *
     * @param \Hometeacher\Models\Users
     * @throws Exception
     */
    public function checkUserFlags(Users $user)
    {
        if ($user->active != '1') {
            throw new Exception('The user is inactive');
        }
    }

    /**
     * Returns the current identity
     *
     * @return array
     */
    public function getIdentity()
    {
        return $this->session->get('auth-identity');
    }

    /**
     * Returns the current identity
     *
     * @return string
     */
    public function getName()
    {
        $identity = $this->session->get('auth-identity');
        return $identity['name'];
    }

    /**
     * Removes the user identity information from session
     */
    public function remove()
    {
        $this->session->remove('auth-identity');
    }


    /**
     * Get the entity related to user in the active identity
     *
     * @return \Hometeacher\Models\Users
     * @throws Exception
     */
    public function getUser()
    {
        $identity = $this->session->get('auth-identity');
        if (isset($identity['id'])) {

            $user = Users::findFirst($identity['id']);
            if ($user == false) {
                throw new Exception('The user does not exist');
            }

            return $user;
        }

        return false;
    }
}
