<?php
namespace Hometeacher\Models;

use Phalcon\Mvc\Model;

/**
 * FailedLogins
 * This model registers unsuccessfull logins registered and non-registered users have made
 */
class FailedLogins extends Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $user_id;

    /**
     *
     * @var string
     */
    public $ip_address;

    /**
     *
     * @var integer
     */
    public $attempted;

    public function initialize()
    {
        $this->belongsTo(
            'user_id',
            'Hometeacher\Models\Users',
            'user_id',
            [
                'alias' => 'user'
            ]
        );
    }
}
