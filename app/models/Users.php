<?php
namespace Hometeacher\Models;

use Phalcon\Mvc\Model;

class Users extends Model
{
    public $user_id;
    public $unit_id;
    public $name;
    public $email;
    public $username;
    public $password;
    public $active;
    public $mustChangePassword;
    public $created_date;

    public function initialize()
    {
        $this->belongsTo(
            'unit_id',
            'Hometeacher/Models/Units',
            'unit_id',
            [
                'alias'     => 'Organization',
                'reusable'  => true
            ]
        );
    }

    public function beforeCreate()
    {
        $this->created_date = date('Y-m-d h:m:s');
    }

}