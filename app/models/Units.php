<?php
namespace Hometeacher\Models;

use Phalcon\Mvc\Model;

class Units extends Model
{
    /**
     * There are 3 different types of units. Stake, Ward and Organization.
     * Organization refers to Elder's Quorum, High Priest or Relief Society
     * Stakes have a unt_unit_id = 0
     * Wards have a unt_unit_id = Stake - unt_id
     * Organizations have unt_unit_id = Ward - unt_id
     */
    const TYPE_STAKE = 1;
    const TYPE_WARD = 2;
    const TYPE_ORGANIZATION = 3;

    public $unit_id;
    public $unit_unit_id;
    public $name;
    public $number;
    public $type_flag;
    public $created_date;

    public $prefix = 'unt';

    public function initialize()
    {
        // Some units can have multiple children (Stakes have many Wards)
        $this->hasMany(
            'unit_id',
            '\Hometeacher\Models\Units',
            'unit_unit_id',
            [
                'alias'     => 'Children',
                'reusable'  => true
            ]
        );

        // Some units only have one parent (ward has one stake)
        $this->hasOne(
            'unit_unit_id',
            '\Hometeacher\Models\Units',
            'unit_id',
            [
                'alias'     => 'Parent',
                'reusable'  => true
            ]
        );

        // Wards have users
        $this->hasMany(
            'unit_id',
            '\Hometeacher\Models\Users',
            'unit_id',
            [
                'alias' => 'User',
                'resuable'  => true
            ]
        );
    }

    public function beforeCreate()
    {
        $this->created_date = date('Y-m-d H:i:s');
    }
}